import { pizzaSelectSize, pizzaSelectTopping, validate, dragData, table, draggable, banner, moyseHover } from "./functions.js";
import { pizzaUser } from "./data-pizza.js"

// перевірка форми
document.querySelectorAll(".grid input")
    .forEach((input) => {
        if (input.type === "text" || input.type === "tel" || input.type === "email") {
            input.addEventListener("change", () => {
                if (input.type === "text" && validate(/^[А-я-іїґє]{2,}$/i, input.value)) {
                    input.className = ""
                    input.classList.add("success")
                    pizzaUser.userName = input.value
                } else if (input.type === "tel" && validate(/^\+380\d{9}$/, input.value)) {
                    input.className = ""
                    input.classList.add("success")
                    pizzaUser.userPhone = input.value
                } else if (input.type === "email" && validate(/^[a-z0-9_.]{3,}@[a-z0-9._]{2,}\.[a-z.]{2,9}$/i, input.value)) {
                    input.className = ""
                    input.classList.add("success")
                    pizzaUser.userEmail = input.value
                } else {
                    input.classList.add("error")
                }
            })
        } else if (input.type === "reset") {
            input.addEventListener("click", () => {
                document.getElementById('info').reset();
            })
        } else if (input.type === "button") {
            input.addEventListener("click", () => {
                console.log(pizzaUser)
                localStorage.userInfo = JSON.stringify(pizzaUser);
                location.href = "./thank-you.html"

            })
        }
    })
// вибір розміру піци
document.querySelector("#pizza")
    .addEventListener("click", pizzaSelectSize)

// вибір компонентів піци
draggable.forEach(el => {
    el.addEventListener('dragstart', dragData)
})

table.addEventListener("dragover", function (evt) {
    if (evt.preventDefault) evt.preventDefault();
    return false;
}, false);
table.addEventListener("drop", pizzaSelectTopping)

//переміщення знижки
banner.addEventListener('mouseenter', moyseHover)

