import { pizzaUser, pizzaBD } from "./data-pizza.js";

const table = document.querySelector(".table")
const [...draggable] = document.querySelectorAll(".draggable")


//вибір розміру піци
const pizzaSelectSize = (e) => {
    if (e.target.tagName === "INPUT" && e.target.checked) {
        pizzaUser.size = pizzaBD.size.find(el => el.name === e.target.id);

    }
    show(pizzaUser)
};

//вибір компонентів піци
const dragData = (e) => {
    e.dataTransfer.setData("Id", e.target.id);
    e.dataTransfer.setData("Src", e.target.src);
}
const pizzaSelectTopping = (e) => {

    let id = e.dataTransfer.getData("Id")
    switch (id) {
        case "sauceClassic": pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === id);

            break;
        case "sauceBBQ": pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === id);

            break;
        case "sauceRikotta": pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === id);

            break;
        case "moc1": pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === id));
            break;
        case "moc2": pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === id));
            break;
        case "moc3": pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === id));
            break;
        case "telya": pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === id));
            break;
        case "vetch1": pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === id));
            break;
        case "vetch2": pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === id));
            break;
    }

    show(pizzaUser);
    let src = e.dataTransfer.getData("Src")

    //перевіряємо чи вибраний уже соус
    const [...tableImg] = document.querySelectorAll(".table img")
    if (id.includes('sauce')) {
        tableImg.forEach(e => {

            if (e.id.includes('sauce')) {
                e.remove()
            }
        })
    }
    table.insertAdjacentHTML("beforeend", `<img src="${src}" id="${id}">`)

}

// формування ціни з вибраних компонентів
function show(pizza) {
    const price = document.getElementById("price");
    const sauce = document.getElementById("sauce");
    const topping = document.getElementById("topping")


    let totalPrice = 0;
    if (pizza.size !== "") {
        totalPrice += parseFloat(pizza.size.price);

    }
    if (pizza.sauce !== "") {
        totalPrice += parseFloat(pizza.sauce.price);

    }
    if (pizza.topping.length) {
        totalPrice += pizza.topping.reduce((a, b) => a + b.price, 0)
    }
    price.innerText = totalPrice;

    if (pizza.sauce !== "") {
        sauce.innerHTML = `<span  class="topping">${pizza.sauce.productName}</span>`
    }

    if (Array.isArray(pizza.topping)) {

        topping.innerHTML = pizza.topping.map(el => `<span class="topping">${el.productName}  </span>`).join("")



    }

    pizzaUser.price = totalPrice;
    pizzaUser.data = new Date()

}


//переміщення знижки
const banner = document.querySelector("#banner  button")
const random = (min, max) => {
    const rand = min + Math.random() * (max - min + 1);
    return Math.floor(rand);
}
const moyseHover = (e) => {

    e.target.parentNode.style.position = "absolute"
    e.target.parentNode.style.right = `${random(0, 90)}%`;
    e.target.parentNode.style.bottom = `${random(0, 90)}%`;

}


// перевірка даних за допомогою регулярного виразу
const validate = (pattern, value) => pattern.test(value);


export { pizzaSelectSize, pizzaSelectTopping, show, validate, dragData, table, draggable, banner, moyseHover }